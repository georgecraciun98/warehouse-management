package bll.validators;


import model.Product;

public class ProductQuantityValidator implements Validator<Product> {
    public void validate(Product t) {

        if (t.getPrice()<=0) {
            throw new IllegalArgumentException("Price is not valid!");
        }
    }

}
