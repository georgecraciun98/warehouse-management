package bll.validators;

import model.Client;

public class ClientAgeValidator implements Validator<Client> {


   private static final int MIN_AGE=10;
   private static final int MAX_AGE=100;

   public void validate(Client x){
       if(x.getAge()<MIN_AGE||x.getAge()>MAX_AGE){
           throw new IllegalArgumentException("Client age limit is not respected ");
       }

   }

}
