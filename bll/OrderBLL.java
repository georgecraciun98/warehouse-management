package bll;

import bll.validators.OrderClientValidator;
import bll.validators.Validator;
import dao.OrderDAO;
import model.Client;
import model.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class OrderBLL {

    private List<Validator<Order>> validators;

    public OrderBLL() {
        validators = new ArrayList<>();
        validators.add(new OrderClientValidator());

    }

    public Order findOrderById(int id) {
        Order st = OrderDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertOrder(Client client,int idproduct,int quantity) {
        Order o = new Order(client.getClientId(),idproduct,quantity);
        for (Validator<Order> v : validators) {
            v.validate(o);
        }
        return OrderDAO.insert(client,idproduct,quantity);
    }


}
