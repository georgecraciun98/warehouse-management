package bll;

import bll.validators.ClientAgeValidator;
import bll.validators.ClientEmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ClientBLL {


    private List<Validator<Client>> validators;

    public ClientBLL() {
        validators = new ArrayList<>();
        validators.add(new ClientEmailValidator());
        validators.add(new ClientAgeValidator());
    }

    public Client findClientById(int id) {
        Client st = ClientDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The student with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertClient(Client client) {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        return ClientDAO.insert(client);
    }
    public void update(String name,String address,String email,int age,int id){
      Client client=new  Client(name,address,email,age);
      for(Validator<Client>v:validators){
          v.validate(client);
      }
      ClientDAO.update( name,address, email, age, id);

    }
    public void delete(int id){
        ClientDAO.delete(id);
    }

}
