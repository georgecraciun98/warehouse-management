package bll;

import bll.validators.ProductQuantityValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ProductBLL {

    private List<Validator<Product>> validators;

    public ProductBLL(){
        validators=new ArrayList();
        validators.add(new ProductQuantityValidator());
    }

    public Product findProductById(int id) {
        Product st = ProductDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The Product with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertProduct(Product product) {
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        return ProductDAO.insert(product);
    }
    public void update(int id,String name,int price,int quantity){
        Product product=new Product(name,price,quantity);
        for(Validator<Product>v:validators){
            v.validate(product);
        }
        ProductDAO.update( id,name,price,quantity);

    }
    public void delete(int id){
        ProductDAO.delete(id);
    }

}
