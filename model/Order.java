package model;

public class Order {
    private int idOrder;
    private int idClient;
    private int totalBill ;
    private int idproduct;
    private int quantity;
    public Order(int a, int b,int c,int d) {
        idOrder = a;
        idClient = b;
        totalBill=0;
        idproduct=c;
        quantity=d;
    }

    public Order(int idClient,int b,int c) {

        this.idClient = idClient;
        idproduct=b;
        quantity=c;
    }

    public int getIdOrder() {
        return idOrder;
    }


    public int getIdClient() {
        return idClient;
    }
    public int getTotalBill() {
        return totalBill;
    }

    public void IncreasetotalBill(int a) {
        totalBill += a;
    }

    public String toString() {
        return "Order [id=" + idOrder + ", idClient=" +idClient+ "total cost= " + totalBill + "]";
    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
