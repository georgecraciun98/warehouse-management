package model;

public class Product {
    private int idProduct;
    private String productName;
    private int price;
    private int quantity;

    public Product(int a,String b,int c,int d){
        idProduct=a;
        productName=b;
        price=c;
        quantity=d;
    }
    public Product(String a,int b,int c){
        productName=a;
        price=b;
        quantity=c;
    }

    public int getIdProduct() {
        return idProduct;
    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public String toString() {
        return "Product [ id=" + idProduct + ", name=" + productName +" price= "+price+" quantity="+quantity+ "]";
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
