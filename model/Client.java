package model;

public class Client {
private int idClient;
private String name;
private String address;
private String email;
private int age;

public Client(int a,String b,String c,String d,int e){
    idClient=a;
    name=b;
    address=c;
    email=d;
    age=e;
}
public Client(String b,String c,String d,int e){
        name=b;
        address=c;
        email=d;
        age=e;
}
public int  getClientId(){
    return idClient;
}
public String getClientName(){
    return name;
}
public String getClientAddress(){
    return address;
}
public String getClientEmail(){
    return email;
}
public int getAge(){
    return age;
}
    public String toString() {
        return "Student [id=" + idClient + ", name=" + name + ", address=" + address + ", email=" + email + ", age=" + age
                + "]";
    }

    public void setClientName(String ClientName) {
        this.name = ClientName;
    }
    public void setClientAddress(String ClientAddress) {
        this.address = ClientAddress;
    }
    public void setClientEmail(String ClientEmail) {
        this.email = ClientEmail;
    }
    public void setClientAge(int age) {
        this.age = age;
    }



}
