package dao;

import connection.ConnectionFactory;
import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductDAO{

    private static final String insertStatementString="INSERT INTO product(productName,price,quantity)"+
            "VALUES(?,?,?) ";
    private static final String findStatementString=" SELECT * FROM product where idProduct = ?";
    private static final String updateStatementString="UPDATE product set productName= ?,price= ?, quantity = ? WHERE idProduct =?";
    private static final String deleteStatementString=" DELETE FROM product WHERE idProduct = ? ";
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    public ArrayList<Product> getResultSet(){
        Connection dbConnection =ConnectionFactory.getConnection();
        PreparedStatement getStatement =null;
        ResultSet rs=null;
        ArrayList<Product> products=new ArrayList<Product>();
        try{
            getStatement=dbConnection.prepareStatement("SELECT idProduct,productName,price,quantity FROM PRODUCT");
            rs=getStatement.executeQuery();
            while(rs.next()) {
                int id=rs.getInt("idProduct");
                String name=rs.getString("productName");
                int price =rs.getInt("price");
                int quantity=rs.getInt("quantity");
                Product c=new Product(id,name,price,quantity);
                products.add(c);
            }

        }
        catch(SQLException e){
            LOGGER.log(Level.WARNING,"ResultSet " + e.getMessage());
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(getStatement);
            ConnectionFactory.close(dbConnection);
        }
        return products;
    }
    public static Product findById(int ProductID){

        Product c=null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement =null;
        ResultSet rs=null;
        try{
            findStatement=dbConnection.prepareStatement(findStatementString);
            findStatement.setInt(1,ProductID);
            rs=findStatement.executeQuery();
            rs.next();

            String name=rs.getString("productName");
            int price=rs.getInt("price");
            int quantity=rs.getInt("quantity");
            c=new Product(ProductID,name,price,quantity);

        }
        catch(SQLException e){
            LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return c;


    }
    public static int insert(Product product) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, product.getProductName());
            insertStatement.setInt(2, product.getPrice());
            insertStatement.setInt(3,product.getQuantity());

            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            System.out.println("Product was inserted ");
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static void update(int id,String productName,int price,int quantity){

        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement updateStatement = null;
        try {

            updateStatement=dbConnection.prepareStatement(updateStatementString);
            updateStatement.setString(1,productName);
            updateStatement.setInt(2,price);
            updateStatement.setInt(3,quantity);
            updateStatement.setInt(4,id);

            updateStatement.executeUpdate();
            System.out.println("Product was Updated ");


        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }

    }
    public static void delete(int id){

        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement updateStatement = null;
        try {

            updateStatement=dbConnection.prepareStatement(deleteStatementString);
            updateStatement.setInt(1,id);
            updateStatement.executeUpdate();
            System.out.println("Product was deleted ");

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }

    }


}
