package dao;

import connection.ConnectionFactory;
import model.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientDAO {

   private static final String insertStatementString="INSERT INTO client(name,address,email,age)"+
           "VALUES(?,?,?,?) ";
   private static final String findStatementString=" SELECT * FROM client where idClient = ?";

    private static final String updateStatementString=" UPDATE client SET name = ? ,address=?,email=?,age=? WHERE idClient = ? ";
    private static final String deleteStatementString=" DELETE FROM client WHERE idClient = ? ";
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());

    public ArrayList<Client> getResultSet(){
        Connection dbConnection =ConnectionFactory.getConnection();
        PreparedStatement getStatement =null;
        ResultSet rs=null;
       ArrayList<Client> clients=new ArrayList<Client>();
        try{
            getStatement=dbConnection.prepareStatement("SELECT * FROM CLIENT");
            rs=getStatement.executeQuery();
            while(rs.next()) {
                int id=rs.getInt("idClient");
                String name=rs.getString("name");
                String address=rs.getString("address");
                String email =rs.getString("email");
                int age=rs.getInt("age");
                Client c=new Client(id,name,address,email,age);
                clients.add(c);
            }

        }
        catch(SQLException e){
            LOGGER.log(Level.WARNING,"ResultSet " + e.getMessage());
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(getStatement);
            ConnectionFactory.close(dbConnection);
        }
        return clients;
    }
   public static Client findById(int ClientId){

       Client c=null;

       Connection dbConnection =ConnectionFactory.getConnection();
       PreparedStatement findStatement =null;
       ResultSet rs=null;
       try{
           findStatement=dbConnection.prepareStatement(findStatementString);
           findStatement.setLong(1,ClientId);
           rs=findStatement.executeQuery();
           rs.next();

           String name=rs.getString("name");
           String address=rs.getString("address");
           String email =rs.getString("email");
           int age=rs.getInt("age");
           c=new Client(ClientId,name,address,email,age);

       }
       catch(SQLException e){
           LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
       }finally {
           ConnectionFactory.close(rs);
           ConnectionFactory.close(findStatement);
           ConnectionFactory.close(dbConnection);
       }
       return c;


   }
    public static int insert(Client client) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, client.getClientName());
            insertStatement.setString(2, client.getClientAddress());
            insertStatement.setString(3, client.getClientEmail());
            insertStatement.setInt(4, client.getAge());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
            System.out.println("Client was inserted ");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
  public static void update(String name,String address,String email,int age,int id){

      Connection dbConnection = ConnectionFactory.getConnection();

      PreparedStatement updateStatement = null;
      try {

          updateStatement=dbConnection.prepareStatement(updateStatementString);
          updateStatement.setString(1,name);
          updateStatement.setString(2,address);
          updateStatement.setString(3,email);
          updateStatement.setInt(4,age);
          updateStatement.setInt(5,id);
          updateStatement.executeUpdate();
          System.out.println("Client was updated");




      } catch (SQLException e) {
          LOGGER.log(Level.WARNING, "ClientDAO:update " + e.getMessage());
      } finally {
          ConnectionFactory.close(updateStatement);
          ConnectionFactory.close(dbConnection);
      }

  }
    public static void delete(int id){

        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement updateStatement = null;
        try {

            updateStatement=dbConnection.prepareStatement(deleteStatementString);

            updateStatement.setInt(1,id);
            updateStatement.executeUpdate();
            System.out.println("Client was deleted ");

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }

    }
  }

