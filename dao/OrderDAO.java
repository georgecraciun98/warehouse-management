package dao;

import bll.ProductBLL;
import connection.ConnectionFactory;
import model.Client;
import model.Order;
import model.Product;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OrderDAO {

    private static final String findStatementString=" SELECT * FROM `ordermanagement`.`order` where idOrder = ?";
    private static final String insertStatementString="INSERT INTO `ordermanagement`.`order`(idClient,idproduct,quantity,totalBill)"+ "VALUES(?,?,?,?) ";

    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    public static Order findById(int OrderID){

        Order c=null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement =null;
        ResultSet rs=null;
        try{
            findStatement=dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1,OrderID);
            rs=findStatement.executeQuery();
            rs.next();


            int idClient=rs.getInt("idClient");
            int totalBill=rs.getInt("totalBill");
            int idproduct=rs.getInt("idproduct");
            int quantity=rs.getInt("quantity");
            c=new Order(OrderID,idClient,idproduct,quantity);

        }
        catch(SQLException e){
            LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return c;


    }
    public static int insert(Client client,int idproduct,int quantity) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setInt(1, client.getClientId());
            insertStatement.setInt(2,idproduct);
            insertStatement.setInt(3,quantity);
            ProductBLL x=new ProductBLL();
            int y=x.findProductById(idproduct).getPrice();
            int sum=y*quantity;
            insertStatement.setInt(4,sum);
            if(x.findProductById(idproduct).getQuantity()>quantity)
            {  insertStatement.executeUpdate();
            Product product=x.findProductById(idproduct);
            x.update(idproduct,product.getProductName(),product.getPrice(),product.getQuantity()-quantity);
            ResultSet rs = insertStatement.getGeneratedKeys();
                System.out.println("Order was inserted ");
            if (rs.next()) {
                insertedId = rs.getInt(1);

            }}
            else if(x.findProductById(idproduct).getQuantity()==quantity){
                insertStatement.executeUpdate();
                x.delete(idproduct);
                ResultSet rs = insertStatement.getGeneratedKeys();
                System.out.println("Order was inserted ");
            }
            else
            {
                System.out.println("Under stock ");
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }


    }



