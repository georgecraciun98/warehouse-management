package presentation;

import dao.ClientDAO;
import dao.ProductDAO;
import model.Client;
import model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame {
    final static String ClientPanel = "Tab with Client";
    final static String ProductPanel = "Tab with Product";
    JPanel p1 = new JPanel();
    JPanel container = new JPanel();
    JFrame frame = new JFrame("Products");
    GridLayout grid = new GridLayout(7, 3, 8, 6);
    CardLayout c1 = new CardLayout();
    JButton b1 = new JButton("Insert Client");
    JButton b2 = new JButton("Edit Client");
    JButton b3 = new JButton("Delete Client");
    JButton b4 = new JButton("Find Client by Id");
    JTextField t1 = new JTextField("Id");
    JTextField t2 = new JTextField("Name");
    JTextField t3 = new JTextField("Address");
    JTextField t4 = new JTextField("Email");
    JTextField t5 = new JTextField("Age");
    JButton b5 = new JButton("Switch to product panel");
    JTextField t6 = new JTextField();
    JTextField t7 = new JTextField();
    JTextField t8 = new JTextField();
    JTextField t9 = new JTextField();
    JTextField t10 = new JTextField();
    ClientDAO y=new ClientDAO();
    ProductDAO y1=new ProductDAO();
    JSpinner spinner=new JSpinner();

    JList<Client> list =new JList(y.getResultSet().toArray());
    JList<Product> products;
    JButton b11=new JButton("Make order");

    GridLayout grid1 = new GridLayout(5, 3, 8, 6);
    JPanel p2 = new JPanel();
    JButton b7=new JButton("Insert Product");
    JButton b8 = new JButton("Edit Product");
    JButton b9 = new JButton("Delete Product");
    JButton b10 = new JButton("Find Product by Id");
    JButton b6 = new JButton("Switch to Client panel");
    JTextField t11 = new JTextField("Id Product");
    JTextField t12 = new JTextField("Product Name");
    JTextField t13 = new JTextField("Price");
    JTextField t14 = new JTextField();
    JTextField t15 = new JTextField();
    JTextField t16 = new JTextField();
    JTextField t17 = new JTextField("Quantity");
    JTextField t18 = new JTextField();

    public View() {
        products=new JList(y1.getResultSet().toArray());
        container.setLayout(c1);
        p1.setLayout(grid);
        t1.setEditable(false);
        t2.setEditable(false);
        t3.setEditable(false);
        t4.setEditable(false);
        t5.setEditable(false);
        t11.setEditable(false);
        t12.setEditable(false);
        t13.setEditable(false);
        t17.setEditable(false);

        p1.add(t1);
        p1.add(t6);
        p1.add(b1);
        p1.add(t2);
        p1.add(t7);
        p1.add(b2);
        p1.add(t3);
        p1.add(t8);
        p1.add(b3);
        p1.add(t4);
        p1.add(t9);
        p1.add(b4);
        p1.add(t5);
        p1.add(t10);
        p1.add(b5);
        p1.add(list);
        p1.add(products);
        p1.add(spinner);
        p1.add(b11);

        p2.setLayout(grid1);
        p2.add(t11);
        p2.add(t14);
        p2.add(b7);
        p2.add(t12);
        p2.add(t15);
        p2.add(b8);
        p2.add(t13);
        p2.add(t16);
        p2.add(b9);
        p2.add(t17);
        p2.add(t18);
        p2.add(b10);
        p2.add(b6);

        container.add(p1, "1");
        container.add(p2, "2");
        c1.show(container, "1");
        frame.add(container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void addb1Listener(ActionListener x) {
        b1.addActionListener(x);
    }

    void addb2Listener(ActionListener x) {
        b2.addActionListener(x);
    }

    void addb3Listener(ActionListener x) {
        b3.addActionListener(x);
    }

    void addb4Listener(ActionListener x) {
        b4.addActionListener(x);
    }

    void addb5Listener(ActionListener x) {
        b5.addActionListener(x);
    }

    void addb6Listener(ActionListener x) {
        b6.addActionListener(x);
    }
    void addb7Listener(ActionListener x) {
        b7.addActionListener(x);
    }
    void addb8Listener(ActionListener x) {
        b8.addActionListener(x);
    }
    void addb9Listener(ActionListener x) {
        b9.addActionListener(x);
    }
       void addb10Listener(ActionListener x) {
        b10.addActionListener(x);
    }

    void addb11Listener(ActionListener x) {
        b11.addActionListener(x);
    }




}
