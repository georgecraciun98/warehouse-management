package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Product;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    private View view;

   public Controller( View v) {
        view = v;
        view.addb1Listener(new b1Listener());
        view.addb2Listener(new b2Listener());
        view.addb3Listener(new b3Listener());
        view.addb4Listener(new b4Listener());
       view.addb5Listener(new b5Listener());
       view.addb6Listener(new b6Listener());
       view.addb7Listener(new b7Listener());
       view.addb8Listener(new b8Listener());
       view.addb9Listener(new b9Listener());
       view.addb10Listener(new b10Listener());
       view.addb11Listener(new b11Listener());

   }


    public class b1Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            try {

            String name=view.t7.getText();
            String address=view.t8.getText();
            String email=view.t9.getText();
            int age=Integer.parseInt(view.t10.getText());
            ClientBLL x=new ClientBLL();
            Client client=new Client(name,address,email,age);
            int id=x.insertClient(client);
            System.out.println("Client with id=" +id+"was Inserted");
            } catch (NumberFormatException nfex) {

            }
        }
    }
    class b2Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {int id=Integer.parseInt(view.t6.getText());
                String name=view.t7.getText();
                String address=view.t8.getText();
                String email=view.t9.getText();
                int age=Integer.parseInt(view.t10.getText());
                ClientBLL x=new ClientBLL();
                x.update(name,address,email,age,id);
                System.out.println("Client with id=" +id+"was Updated");


            } catch (NumberFormatException nfex) {

            }
        }
    }
    class b3Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {int id=Integer.parseInt(view.t6.getText());

                ClientBLL x=new ClientBLL();
                x.delete(id);
                System.out.println("Client with id=" +id+"was Deleted");



            } catch (NumberFormatException nfex) {

            }
        }
    }
    class b4Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {

                int id=Integer.parseInt(view.t6.getText());

                ClientBLL x=new ClientBLL();
                Client client=x.findClientById(id);
                if(client!=null)
                System.out.println("Client with id=" +id+"was Found");
                else
                    System.out.println("Client with id=" +id+"was not Found");



            } catch (NumberFormatException nfex) {

            }
        }
    }
    class b5Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {
                view.c1.show(view.container,"2");


            } catch (NumberFormatException nfex) {

            }
        }

    }
    class b6Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {
                view.c1.show(view.container,"1");


            } catch (NumberFormatException nfex) {

            }
        }
    }
    class b7Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {
                String name=view.t15.getText();
                int price=Integer.parseInt(view.t16.getText());
                int quantity=Integer.parseInt(view.t18.getText());
                ProductBLL x=new ProductBLL();
                Product product=new Product(name,price,quantity);
                int id=x.insertProduct(product);
                System.out.println("Product with id=" +id+"was Inserted");

            } catch (NumberFormatException nfex) {

            }
        }
    }
    class b8Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {
                int id=Integer.parseInt(view.t14.getText());
                String name=view.t15.getText();
                int quantity=Integer.parseInt(view.t18.getText());
                int price=Integer.parseInt(view.t16.getText());
                ProductBLL x=new ProductBLL();
                Product product=new Product(name,price,quantity);
                x.update(id,name,price,quantity);
                System.out.println("Product with id=" +id+"was Updated");


            } catch (NumberFormatException nfex) {

            }
        }
    }
    class b9Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {
                int id=Integer.parseInt(view.t14.getText());

                ProductBLL x=new ProductBLL();
                x.delete(id);
                System.out.println("Product with id=" +id+"was Deleted");



            } catch (NumberFormatException nfex) {

            }
        }
    }
    class b10Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {
                int id=Integer.parseInt(view.t14.getText());

                ProductBLL x=new ProductBLL();
                Product product=x.findProductById(id);
                if(product!=null)
                    System.out.println("Product with id=" +id+"was Found");
                else
                    System.out.println("Product with id=" +id+"was not Found");




            } catch (NumberFormatException nfex) {

            }
        }
    }
    class b11Listener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            try {
             if(view.list.getSelectedValue()!=null&&view.products.getSelectedValue()!=null&&(Integer)view.spinner.getValue()>0)
             {
              OrderBLL order=new OrderBLL();
              order.insertOrder(view.list.getSelectedValue(),view.products.getSelectedValue().getIdProduct(),(Integer)view.spinner.getValue());
             }

            } catch (NumberFormatException nfex) {

            }
        }
    }


}




